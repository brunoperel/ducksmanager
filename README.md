## DucksManager

DucksManager est un site Web gratuit et open-source offrant de nombreux outils utiles aux collectionneurs de bandes dessinées Disney.

Envie d'en savoir plus sur les dessous de DucksManager ? [Accédez au wiki](https://github.com/ducksmanager/DucksManager/wiki)
